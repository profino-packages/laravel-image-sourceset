# Image Sourceset

### Status

1.1.4 - in development

<br />

### About this package

**This package is work in progress. Nothing in this repository is production ready!**

This package tries to find some relieve in producing [srcset](https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images) strings. Everything combined with automatic generating of those images in each desired size.

It depends heavily on a beautiful peace of code called [Image Intervention](image.intervention.io). If you're not familiar with it, go ahead and take a look.

In order to get these nice responsive images to work all across my projects, I had to build some crazy complicated foreach loops to produce just one single line. And if that wasn't just enough, some developers started to ask questions if they had to execute these command lines to regenerate the thumbnails everytime they wanted to experiment with different sizes of an image.

My answer was always *yes*.

After trying several well known packages that deals with the same topic of generating images and trying to solve everything you might come across with, I got quite disappointed and decided to build something smaller and simpler that would be really good at doing just one thing.

<br />

### How it works

So imagine we had an image that needed to be responsive in 3 available sizes; `320`, `640` and `1280` pixels width. We could simply tell it to do so by the following line of code:

```php
sourceset($image, [320, 640, 1280]);
```

and `sourceset` would return a `srcset` string formatted with the correct sizes:

```bash
/sizes/image_320.jpg 320w, /sizes/image_640.jpg 640w, /sizes/image_1280.jpg 1280w
```

where each given size results in a generated (constrained) image with a width of that size in a default directory called `sizes`:

```php
- /image.jpg (original)
- /sizes
	- /image_320.jpg
	- /image_640.jpg 
	- /image_1280.jpg
```

<br />

### Filters

In order to tell `sourceset` how to generate images, filters can be applied. The default filter just tells the image to [widen](http://image.intervention.io/api/widen) the image to its desired width.

```php

class DefaultFilter implements FilterInterface {

    protected $resizeable;

    public function __construct($resizeable) {
        $this->resizeable = $resizeable;
    }

    /**
     * Applies filter to given image
     *
     * @param Image $Image
     * @return Image
     */
    public function applyFilter(Image $Image) {
        $Image->widen($this->resizeable->size);
        
        return $Image;
    }

}

```

The `$Image` variable in `applyFilter` is the actual Image Intervention object. Here we can apply everything Image Intervention [has to offer](http://image.intervention.io/use/basics). The `$resizeable` property is the actual image to be resized. For example, if we would like to resize a 640px version of our original image and adjust its height, we could do so:


```php
    public function applyFilter(Image $Image) {
    	// are we at version 640?
    	if($this->resizeable->size === 640) {
    		// .. resize with a different height
			$Image->resize(640, 160);    	
    	}
    	// if not ..
    	else {
    		// .. just default widen
        	$Image->widen($this->resizeable->size);
      	}
      	
   		return $Image;
    }
```

<br />

### Custom Filters

To apply a custom filter for an image, we could simply create a new class called `NewsStage` and extend it with the `DefaultFilter` class. 

Assign it to the `filters => []` array in a `shortcut` / `classname` setup, available in `config/sourceset.php`:

```php
	'filters' => [
		'news:stage' => 'App\ImageSourceset\Filters\NewsStage.php'
	]
```

```php
namespace App\ImageSourceset\Filters;

use WizeWiz\ImageSourceset\Filters\DefaultFilter;

/**
 * A filter for a big stage image used in our news section.
 */ 
class NewsStage extends DefaultFilter {

    /**
     * Apply custom filter stage image
     *
     * @param Image $Image
     * @return Image
     */
    public function applyFilter(Image $Image) {
    	// all goes here ..
    	 
    	return $Image;
    }

}

```

In order to use this filter, we add it as a third parameter in the `sourceset` helper function:

```php
sourceset($imagefile, [320, 640], 'news:stage');
```
<br />

### Modes

In order to keep performance and laziness in line, sourceset knows 2 modes; `verify` and `confident`.

#### verify mode

The `verify` mode verifies each and every size, either given as an argument, or retrieved from the database/cache. Here you could wildly delete images or flush database entries and everything gets generated on next load. It skips the use of a cache and retrieves fresh entries from the database. Great for local and development environments.

#### confident mode

Now the opposite is the `confident` mode. Here only the given sizes are checked with the database entry and, if they do not match, generates only the sizes missing. It assumes sizes previously generated (retrieved from the database entry) exist. The cache only flushes itself after expiration and only retrieves a new entry from the database after the cache expired. Optimal for production environments where images do not get deleted by accident.

<br />

### TODO's

- make this work really well ..
- make this readme cover the entire configuration file.
- visit an english course.
- go ask jeffrey why laravel is so great, but his hair always looks the same.

<br />

### License

The MIT License (MIT). Please see [License File](https://github.com/wize-wiz/laravel-image-sourceset/blob/master/LICENSE.md) for more information.
