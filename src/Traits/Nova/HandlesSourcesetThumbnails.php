<?php

namespace WizeWiz\ImageSourceset\Traits\Nova;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Fields\{
    Avatar,
    Image,
    Text
};
use WizeWiz\ImageSourceset\Contracts\UsesSourcesetImages;
use WizeWiz\ImageSourceset\Models\Sourceset;
use DigitalCreative\Filepond\Filepond;

/**
 * Manages the Image field to automatically render a thumbnail suitable for Nova
 */
trait HandlesSourcesetThumbnails {

    /**
     * Create sourceset callback.
     *
     * @param $model
     * @param $label
     * @param $attribute
     * @param $size
     * @return \Closure
     */
    protected function _createSourcesetCallback($model, $label, $attribute, $size) {
        return function ($image) use ($model, $label, $attribute, $size) {
            if($model && ($thumbnail = $model->generateSourcesetSizes($attribute, $size))) {
                return $thumbnail;
            }
            return $image;
        };
    }

    /**
     * @param $label
     * @param $attribute
     * @param $size
     */
    public function NovaImage($label, $attribute, $size = 32) {
        $model = $this->Handler->getModel();
        $disk = $model !== null ?
            $model->getSourcesetDisk($attribute):
            'local';

        // create Image
        return Image::make($label, $attribute)
            ->disk($disk)
            ->maxWidth($size);

        return $Image;
    }

    /**
     * @param $label
     * @param $attribute
     */
    public function NovaAvatar($label, $attribute) {
        $size = 32;
        $model = $this->Handler->getModel();
        $disk = $model !== null ?
            $model->getSourcesetDisk($attribute):
            'local';

        // create Image
        return Avatar::make($label, $attribute)
            ->disk($disk)
            ->thumbnail($this->_createSourcesetCallback($model, $label, $attribute, $size))
            ->preview($this->_createSourcesetCallback($model, $label, $attribute, $size));
    }

    /**
     * Manage Novas Image field.
     *
     * @param $label
     * @param $attribute
     * @param $size Size of the thumbnail, 32, 64 or 128
     * @return mixed
     */
    public function NovaSourcesetImage($label, $attribute, $size = 32) {
        $model = $this->Handler->getModel();
        $size = (int) $size;
        $disk = $model->getSourcesetDisk($attribute);

        // create Image
        $Image = Image::make($label, $attribute)
                ->disk($disk);

        // index
        if($size <= 32) {
            $Image->thumbnail($this->_createSourcesetCallback($model, $label, $attribute, $size));
        }
        // detail, update and create
        elseif($size > 32) {
            $Image->preview($this->_createSourcesetCallback($model, $label, $attribute, $size));
        }

        // store file
        $Image->store(function($request, $model) use ($attribute, $disk) {
            // file attached
            $attachment = $request->{$attribute};
            // hashed filename
            $filename = $attachment->getClientOriginalName();
            // get path and filename separate
            list('path' => $path, 'filename' => $filename) = Sourceset::generateStorePath(Sourceset::generateHashedFileName($filename), true);
            // store file as
            return [
                $attribute => $attachment->storeAs($path, $filename, $disk),
            ];
        });

        // set max width
        $Image->maxWidth($size);

        return $Image;
    }

    /**
     * @param $label
     * @param $attribute
     * @param $type
     * @return mixed
     */
    public function NovaSourcesetFilepond($label, $attribute, $type = 'image') {

        $Handler = $this->getHandler();
        $model = $Handler->getModel();
        $request_type = $Handler->getType();

        $disk = 'local';

        if($request_type === 'detail' && $type === 'image') {
            if($model !== null) {
                if($model->{$attribute} === null) {
                    return Text::make($label, function() use($model, $attribute) {
                        return "kein Bild vorhanden.";
                    });
                }
                elseif($model instanceof UsesSourcesetImages && $model->hasSourcesetSizesProperty($attribute)) {
                    $disk = $model->getSourcesetDisk($attribute);

                    if(Storage::disk($disk)->has($model->{$attribute}) === false) {
                        return Text::make($label, function() use($model, $attribute) {
                            return "Bild nicht gefunden ( {$model->{$attribute}} ).";
                        });
                    }
                }
            }
        }

        $Image = Filepond::make($label, $attribute);

        switch($type) {
            case 'image':
                $Image->image();
                if($model !== null && $model instanceof UsesSourcesetImages) {
                    $disk = $model->getSourcesetDisk($attribute);
                }
                break;
            case 'video':
                $Image->video();
                if($model !== null) {
                    // @todo: implement contract to use getDisk
                    $disk = $model->getDisk($attribute);
                }
                break;
            case 'pdf':
                $Image->mimesTypes('application/pdf');
                if($model !== null) {
                    // @todo: implement contract to use getDisk
                    $disk = $model->getDisk($attribute);
                }
                break;
        }

        $Image
            ->disk($disk)
            ->storeAs(function (File $file) {
                $filename = $file->getBasename();
                // get path and filename separate
                list('path' => $path, 'filename' => $filename) = Sourceset::generateStorePath(Sourceset::generateHashedFileName($filename), true);
                return $path.DIRECTORY_SEPARATOR.$filename;
            })
            ->onDelete(function($model, $attribute, $image, $disk) {
                if($model instanceof UsesSourcesetImages) {
                    $hasSourcesetImages = $model->hasSourcesetSizesProperty($attribute);
                    if($hasSourcesetImages) {
                        $model->removeGeneratedSourcesets($attribute);
                    }
                }
            });

        return $Image;
    }

}