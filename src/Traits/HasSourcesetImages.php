<?php

namespace WizeWiz\ImageSourceset\Traits;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use WizeWiz\ImageSourceset\Exceptions\InvalidSourceException;
use WizeWiz\ImageSourceset\Exceptions\InvalidSourcesetProperty;
use WizeWiz\ImageSourceset\Models\Sourceset;

/**
 * Images disks, either a string representing one single disk
 * for all images, or an array with property per disk
 *
 * @var string
 */
trait HasSourcesetImages {

    protected $_sourcesets = [];

    /**
     * protected $sourceset_disk = 'local'
     * protected $sourceset_sizes = []
     */

    protected function getSourcesetComplexParameters($image, $sizes) {
        if($image !== null && is_array($image)) {
            $sizes = $image;
            $image = null;
        }
        if($sizes !== null && !is_array($sizes)) {
            $sizes = [$sizes];
        }
        return [$image, $sizes];
    }

    /**
     * Generate sourceset and return as array
     *
     * @param $property
     * @param null $image
     * @param array|integer $sizes
     * @return array
     * @throws InvalidSourceException
     */
    public function toSourcesetArray($property, $image = null, $sizes = null) {
        list($image, $sizes) = $this->getSourcesetComplexParameters($image, $sizes);
        $Sourceset = $this->generateSourceset($property, $image, $sizes);
        return $Sourceset !== null ? $Sourceset->asArray() : [];
    }

    /**
     * Generate sourceset and return as sourceset string
     *
     * @param $property
     * @param null $image
     * @param array $sizes
     * @return string
     * @throws InvalidSourceException
     */
    public function toSourcesetString($property, $image = null, $sizes = null) : string {
        list($image, $sizes) = $this->getSourcesetComplexParameters($image, $sizes);
        $images = $this->generateSourceset($property, $image, $sizes);
        return $images !== null ? $images->asSourceset() : '';
    }

    /**
     * Generate sourceset and return as sourceset string
     *
     * @param $property
     * @param null $image
     * @return string
     * @throws InvalidSourceException
     */
    public function toSourceset($property, $image = null, $sizes = null) {
        list($image, $sizes) = $this->getSourcesetComplexParameters($image, $sizes);
        return $this->generateSourceset($property, $image);
    }

    /**
     * Generates given sizes and returns the path of resizeables.
     *
     * @param $property
     * @param $sizes
     * @param null $image
     * @return array|mixed|null
     * @throws InvalidSourceException
     */
    public function generateSourcesetSizes($property, $sizes, $image = null) {
        $Sourceset = $this->generateSourceset($property, $image, $sizes);
        // if valid
        if($Sourceset === null) {
            return null;
        }
        $sizes = $Sourceset->fromArray($sizes);

        // if no sizes found, just return original as if nothing happened.
        if(!is_countable($sizes)) {
            return $Sourceset->getRelativePath();
        }

        return  count($sizes) === 1 ? array_shift($sizes) : array_values($sizes);
    }

    /**
     * Generate a sourceset instance or retrieve stored generated sourceset from $_sourcesets.
     *
     * @param $property
     * @param $image Assign different source for image but use given $property setting.
     * @return string
     *
     * @throws InvalidSourceException
     */
    public function generateSourceset($property, $image = null, $sizes = null) {
        // if property (attribute on this model instance) does not exist, we can abort
        if(!array_key_exists($property, $this->attributes)) {
            // we should never throw an exception, e.g. if in Nova a new model is created from resource
            // with Resource::newModel(), the property isn't set. Therefor silently fail by returning null.
            return null;
            // throw new InvalidSourcesetProperty("invalid sourceset property: {$property} not found on model " . get_class($this));
        }

        // get stored sourceset if image was given
        if($image !== null && array_key_exists($image, $this->_sourcesets)) {
            $sourceset = $this->_sourcesets[$image];
        }
        // get stored sourceset if image has to be retrieved from this instance attribute ($property)
        elseif($image === null && array_key_exists($property, $this->_sourcesets)) {
            $sourceset = $this->_sourcesets[$property];
        }
        // generate new sourceset
        else {
            if($image === null && !is_string($this->{$property})) {
                return null;
            }

            $image = ($image === null) ? $this->{$property} : $image;
            $sizes = ($sizes === null) ? $this->getSourcesetSizes($property) : $sizes;

            // store sourceset
            $this->_sourcesets[$image !== null ? $image : $property] =
            $sourceset = sourceset([
                $image,
                $this->getSourcesetDisk($property)
            ],
            $sizes);
        }

        return $sourceset;
    }



    /**
     * Is $this->sourceset_disk defined.
     *
     * @return bool
     */
    public function definedSourcesetDisk() : bool {
        return property_exists($this, 'sourceset_disk');
    }

    /**
     * Is $this->sourceset_disk defined.
     *
     * @return bool
     */
    protected function definedSourcesetSizes() : bool {
        return property_exists($this, 'sourceset_sizes');
    }

    /**
     * Has sourceset property defined for given image.
     *
     * @param $property
     * @return boolean
     */
    public function hasSourcesetSizesProperty($property) {
        return ($this->definedSourcesetSizes() && array_key_exists($property, $this->sourceset_sizes));
    }

    /**
     * Get sourceset disk.
     *
     * @param $property
     */
    public function getSourcesetDisk($property = null) {
        // set default if property was not set
        if(!$this->definedSourcesetDisk()) {
            return 'local';
        }
        // multiple disk given, find property
        if(is_array($this->sourceset_disk)) {
            if(isset($this->sourceset_disk[$property])) {
                return $this->sourceset_disk[$property];
            }

            return isset($this->sourceset_disk['default']) ?
                $this->sourceset_disk['default'] :
                'local';
        }

        return $this->sourceset_disk;
    }

    /**
     * Get sizes for a sourceset by property.
     *
     * @param $property
     * @return array
     */
    public function getSourcesetSizes($property) : array {
        return $this->hasSourcesetSizesProperty($property) ?
            $this->sourceset_sizes[$property] :
            [];
    }

    /**
     * Return relative file path using the disk defined in the model
     *
     * @param $path
     * @return mixed
     */
    public function getFileRelativePath($path) {
        return str_replace(url(''), '', Storage::disk($this->sourceset_disk)->url($path));
    }

    /**
     * Remove any generated sourcesets (cache/db/files).
     *
     * @param $attribute
     */
    public function removeGeneratedSourcesets($attribute) {
        $Sourceset = Sourceset::findOrCreate($this->{$attribute});
        // resizeable exists
        if($Sourceset->id !== null) {
            $disk = Storage::disk(config('sourceset.sizes_disk'));
            foreach($Sourceset->resizeables as $resizeable) {
                if($disk->has($resizeable)) {
                    try {
                        $disk->delete($resizeable);
                    // just ignore
                    } catch(\Exception $e) {}
                }
            }
        }
    }

}
