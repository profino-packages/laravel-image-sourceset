<?php

namespace WizeWiz\ImageSourceset;

use Illuminate\Support\ServiceProvider;

class ImageSourcesetServiceProvider extends ServiceProvider {
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton('image-sourceset.sourceset', function() {
            return new Sourceset();
        });
        $this->app->alias('image-sourceset.sourceset', Sourceset::class);

        $this->mergeConfigFrom(
            __DIR__.'/config.php', 'sourceset'
        );

        // @todo: merge env from config with sourceset-config
    }

    /**
     * Bootstrap App services.
     */
    public function boot() {
        $this->publishes([
            __DIR__.DIRECTORY_SEPARATOR.'config.php' => config_path('sourceset.php'),
        ]);

        $this->loadMigrationsFrom(__DIR__.DIRECTORY_SEPARATOR.'migrations');

        if ($this->app->runningInConsole()) {
            // @todo: commands
            $this->commands([]);
        }
    }
}
