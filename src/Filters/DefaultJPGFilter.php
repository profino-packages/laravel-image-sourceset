<?php

namespace WizeWiz\ImageSourceset\Filters;

use Intervention\Image\Image;

class DefaultJPGFilter extends DefaultFilter {

    public function applyFilter(Image $Image) {
        $Image = parent::applyFilter($Image);
    }

}