<?php

namespace WizeWiz\ImageSourceset\Filters;

use Illuminate\Support\Facades\Log;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

/**
 * Class DefaultFilter optimized for PNG
 * @package WizeWiz\ImageSourceset\Filters
 *
 */
class DefaultFilter implements FilterInterface {

    protected $resizeable;

    public function __construct($resizeable) {
        $this->resizeable = $resizeable;
    }

    /**
     * Applies filter to given image
     *
     * @param Image $Image
     * @return Image
     */
    public function applyFilter(Image $Image) {
        $Image->widen($this->resizeable->size);
        return $Image;
    }

}