<?php

    if(!function_exists('sourceset')) {
        function sourceset($source, $sizes, $filter = null, $mode = null) {
            if(!is_array($sizes)) {
                $sizes = [$sizes];
            }

            return new \WizeWiz\ImageSourceset\Sourceset(
                $source,
                $sizes,
                $filter);
        }
    }