<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageSourcesetsTable extends Migration {

    public function up() {
        Schema::create('image_sourcesets', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('filename');
            $table->string('file');
            $table->json('data')->nullable();
            $table->json('resizeables')->nullable();
            $table->string("filter")->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists("image_sourcesets");
    }

}