<?php

    return [

        /*
        |--------------------------------------------------------------------------
        | Mode
        |--------------------------------------------------------------------------
        |
        | Determines the mode how and when images will be generated. The 'verify'
        | mode will check each image on its existence. In 'confident' mode does the
        | opposite that the generated sizes return by the database are always present
        | and compares generated (db) sizes with given (argument) sizes.
        */

        'mode' => 'verify',

        /*
        |--------------------------------------------------------------------------
        | Sizes Sorting
        |--------------------------------------------------------------------------
        |
        | How the srcset string should be generated, ascending (320, 480, .. etc) or
        | descending (640, 480, .. etc).
        |
        */

        'sort' => 'asc',

        /*
        |--------------------------------------------------------------------------
        | Processing
        |--------------------------------------------------------------------------
        |
        | If the URL of the application should be included. By laravel's application
        | url is used. Use included URL to overwrite this behaviour.
        |
        */

        'always_generate' => false,

        /*
        |--------------------------------------------------------------------------
        | Default image not-found
        |--------------------------------------------------------------------------
        |
        | What image to use when an image was not found
        |
        */
        'image_not_found' => '/storage/image-not-found.jpg',

        /*
        |--------------------------------------------------------------------------
        | Filesystem
        |--------------------------------------------------------------------------
        |
        | Storage disk to use for storing generated sizes. Default is sizes. If
        | 'sizes_disk' is null, the 'local' disk is used.
        |
        */

        'sizes_disk' => 'sizes',

        /*
        |--------------------------------------------------------------------------
        | Processing
        |--------------------------------------------------------------------------
        |
        | If the URL of the application should be included. By laravel's application
        | url is used. Use included URL to overwrite this behaviour.
        |
        */

        'filters' => [],
        'default_filter' => 'WizeWiz\ImageSourceset\Filters\DefaultFilter',

        /*
        |--------------------------------------------------------------------------
        | Post-Processing / Optimizers
        |--------------------------------------------------------------------------
        |
        | Very simple and static optimizers using `pngquant` and `jpegoptim`. Maybe
        | for feature releases use spatie/image-optimizer.
        |
        */
        'use_optimizers' => true,
        'optimizer_png' => [
            'pngquant' => 75    //
        ],
        'optimizer_jpg' => [
            'jpegoptim' => 70   // jpegoptim image.jpg -m70 --strip-all
        ],

        /*
        |--------------------------------------------------------------------------
        | Intervention Image settings
        |--------------------------------------------------------------------------
        |
        | Default Intervention Image settings can be defined here. These will be
        | used passed on to each and every Filter being applied.
        |
        */

        // @todo: flatt options
        'intervention' => [
            // quality only for JPG
            'quality' => 100,
            // this converts the PNG to a JPG if no alpha was detected
            'convert_png' => true
        ],

        /*
        |--------------------------------------------------------------------------
        | Queue
        |--------------------------------------------------------------------------
        |
        | Determines the queue being used for image processing.
        |
        */

        // @todo: flatt options
        'queue' => [
            // on which queue it should be pushed, default default ;)
            'on' => 'default' // @todo: queue_on
        ],

        /*
        |--------------------------------------------------------------------------
        | Cache
        |--------------------------------------------------------------------------
        |
        | Sourcesets entries retrieved from the database are being cached,
        | by default this is enabled and sets a TTL of 60 minutes. The prefix will
        | determine that beginning of each cache key.
        |
        */

        'cache' => [
            'enabled' => true,
            // minutes.
            'remember' => 60,
            // generated as "{key}{filename}".
            'prefix' => 'image:sourceset:',
            // use any tag for cache to be used. This can be used to purge the cache
            // from any generated image present.
            'tag' => 'image-sourceset'
        ],

        /*
        |--------------------------------------------------------------------------
        | Environment
        |--------------------------------------------------------------------------
        |
        | Each environment can be configured here. Every setting for this config
        | can be used in 'local', 'development' or 'production'.
        |
        */

        'env' => [
            'production' => [],
            'local' => []
        ],

        /*
        |--------------------------------------------------------------------------
        | Debugging
        |--------------------------------------------------------------------------
        |
        | Outputs detailed information in the applications log file.
        |
        */

        'verbose' => false,

        /*
        |--------------------------------------------------------------------------
        | Exceptions
        |--------------------------------------------------------------------------
        |
        | Throw exceptions if sources or files are missing. Setting this option
        | to true will ignore any error and always return the original source
        | path as if it were a valid image.
        |
        */

        'silent' => true,
];