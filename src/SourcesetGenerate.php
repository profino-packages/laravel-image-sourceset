<?php

namespace WizeWiz\ImageSourceset;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image as InterventionImage;

use WizeWiz\ImageSourceset\Models\Sourceset as SourcesetModel;
use WizeWiz\ImageSourceset\Filters\DefaultFilter;

class SourcesetGenerate {

    protected $source;
    protected $sizes_disk;
    protected $sizes;
    protected $filter;
    protected $Filter = null;

    protected $generated_sizes = [];

    protected $config = [];
    // @todo: rename to $cache_prefix
    protected $cache_prefix;
    protected $cache;

    protected $image;

    /**
     * SourcesetGenerate constructor.
     * @param Sourceset $source
     * @param array $sizes
     * @param string $filter
     */
    public function __construct(Sourceset $source, array $sizes, string $filter) {
        // set source object
        $this->source = $source;
        // set config
        $this->config = $source->getConfig();
        // sizes, sorting from biggest size to smallest
        $this->sizes = $this->sortSizes($sizes, 'desc');
        $this->filter = $filter;
        $this->setSizesDisk($this->config->sizes_disk);
        // configure cache
        $this->configureCache();

        // get model for sourceset
        $this->image = $this->retrieveSourcesetModel();

        // supply addition data if no record exists
        if($this->image->hasRecord() === false) {
            // any directory included in the filename
            $dirname = $source->getDirname();
            // get relative path
            $relative = str_replace(url(''), '', $this->sizes_disk->url($source->getDirname()));
            // initialize the model
            $this->image->initialize($this->filter, $sizes, [
                'dirname' => $dirname,
                'relative' => $relative,
                'storage' => str_replace($dirname, '', $relative),
            ]);
        }
        // handle generation
        $this->handle();
    }

    /**
     * Configure the cache.
     */
    protected function configureCache() {
        if($this->config->cache['enabled'] !== true)
            return;

        Sourceset::debug('initialize cache: ' . env('CACHE_DRIVER'));
        // @todo: get KEY from config
        $this->cache = cache()->tags([$this->config->cache['tag']]);
        // @todo: check
        $this->cache_prefix = $this->config->cache['prefix'] . pathinfo($this->source->getFilename())['filename'];
        $this->cache_remember = $this->config->cache['remember'];
    }

    /**
     * Set storage disk for sizes.
     *
     * @param $sizes_disk
     */
    protected function setSizesDisk($sizes_disk) {
        try {
            $this->sizes_disk = Storage::disk($sizes_disk);
        } catch(\Exception $e) {
            Log::info($e);
        }
    }

    /**
     * Get sizes file path
     *
     * @param $file
     * @return string
     */
    public function getSizesFilePath($file) {
        return $this->sizes_disk->path($file);
    }

    /**
     * @param array $sizes
     * @return array
     */
    public function sortSizes(array $sizes, $order = 'desc') {
        // sort default to 'desc'
        sort($sizes);
        return array_reverse($sizes);
    }

    /**
     * Check file and generate images if needed
     * @todo: add Job
     */
    protected function handle() {
        Sourceset::debug('start handling: ' . $this->source->getSource());
        // generator to be used, will only be created when generating an image
        $Generator = null;

        // boolean array for generated resizeables
        $generated = [];
        $resizeables_found = 0;

        // set mode on how to handle missing sizes
        switch($this->config->mode) {
            default:
            case 'verify':
                $sizes = $this->sizes;
                break;
            case 'confident':
                $sizes = $this->image->getMissingResizeables($this->sizes);
                break;
        }

        // generate each size if generateable
        foreach($sizes as $size) {

            $resizeable_filename = isset($this->image->resizeables[$size]) ?
                $this->image->resizeables[$size] :
                $this->image->generateResizeableFilename($size, true);

            if(($generated[] = $this->generateable($resizeable_filename)) === true) {

                if($Generator === null) {
                    // @todo: if mode confident and size is missing, improve by getting the next
                    //          biggest size for the missing $size to be generated.
                    $Generator = $this->getGeneratorInstance($this->source->getFilePath());
                }

                // @todo: nothing is done with `$this->generate` return true/false
                if($this->generate($Generator, $size, $resizeable_filename)) {
                    $this->image->setResizeable($size, $resizeable_filename);
                    $resizeables_found++;
                }
            }
            else {
                Sourceset::debug('skipping ' . $size);
                $resizeables_found++;
            }
        }



        // stores the model if ..
        if( // .. generated items have been changed,
            array_sum($generated) ||
            // .. resizeables were found but no record exists in the database.
            ($resizeables_found > 0 && ($hasRecord = $this->image->hasRecord()) === false)) {
                $this->storeSourcesetModel();
        }

        Sourceset::debug('end handling');
        Sourceset::debug('---');
    }

    /**
     * Get instance of intervention image
     *
     * @param string $source
     * @return mixed
     */
    protected function getGeneratorInstance(string $source) {
        return Image::make($source);
    }

    /**
     * Check if resized image needs to be generated
     *
     * @param object $image
     * @param string $resized_filename
     * @return bool
     */
    protected function generateable($filename) : bool {
        return
            // always generate from config
            $this->config->always_generate ||
            // generate if resized image does not exist
            $this->sizes_disk->exists($filename) === false
        ;
    }

    /**
     * Generate image by given given Filter. $Image and $resizeable gets passed on to Filter.
     *
     * @param Intervention\Image\Image $Generator
     * @param object $resizeable
     * @return bool True if image was generated, false otherwise.
     */
    protected function generate(InterventionImage &$Generator, $size, $resizeable_filename) {
        try {
            $Generator->filter($this->makeFilter($this->filter, [
                'size' => $size,
                'filename' => $resizeable_filename,
                'path' => $this->sizes_disk->path('')
            ]));

            // @todo: move to somewhere else
            $this->sizes_disk->makeDirectory(pathinfo($resizeable_filename)['dirname']);
            $resizeable_filename_path = $this->sizes_disk->path($resizeable_filename);
            $Generator->save($resizeable_filename_path);

            // post processing
            if($this->config->use_optimizers === true) {
                $this->postProcess($resizeable_filename_path, $this->source->getExtension());
            }

            Sourceset::debug('generated ' . $resizeable_filename);

            return true;
        } catch(\Exception $e) {
            Sourceset::debug('Exception: SourcesetGenerate@generate');
            Sourceset::debug($e);
            return false;
        }
    }

    /**
     * Create the filter to apply
     *
     * @param string $filter
     * @param object $resizeable
     * @return mixed
     */
    protected function makeFilter(string $filter, array $resizeable) {
        Sourceset::debug('creating filter: ' . $filter);
        return app($filter, ['resizeable' => (object)$resizeable]);
    }

    /**
     * Excellent function by Anubis (https://stackoverflow.com/users/1934919/anubis)
     * @source https://stackoverflow.com/a/43996262
     * @param $file
     * @return bool
     * @todo: refactor for this class
     */
    public function isTransparentPNG($file) {
        // 32-bit pngs
        // 4 checks for greyscale + alpha and RGB + alpha
        if ((ord(file_get_contents($file, false, null, 25, 1)) & 4) > 0) {
            return true;
        }
        // 8 bit pngs
        $fd = fopen($file, 'r');
        $continue = true;
        $plte = false;
        $trns = false;
        $idat = false;
        while ($continue === true) {
            $continue = false;
            $line = fread($fd, 1024);
            if ($plte === false) {
                $plte = (stripos($line, 'PLTE') !== false);
            }
            if ($trns === false) {
                $trns = (stripos($line, 'tRNS') !== false);
            }
            if ($idat === false) {
                $idat = (stripos($line, 'IDAT') !== false);
            }
            if ($idat === false and !($plte === true and $trns === true)) {
                $continue = true;
            }
        }
        fclose($fd);
        return ($plte === true and $trns === true);
    }

    /**
     * Optimizes PNG file with pngquant 1.8 or later.
     *
     * Overwrites existing PNG with compressed version.
     *
     * apt-get install pngquant (linux)
     *
     * @param $file string - path to any PNG file.
     * @param $max_quality int - conversion quality, useful values from 60 to 100 (smaller number = smaller file).
     * @param $min_quality int - minimal conversation quality.
     * @return mixed
     */
    public function compressPng($file, $max_quality = 100, $min_quality = 50) {
        // '-' makes it use stdout, required to save to $compressed_png_content variable
        // '<' makes it read from the given file path
        // escapeshellarg() makes this safe to use with any path
        $file = escapeshellarg($file);
        return shell_exec("pngquant --ext=.png --force --quality=$min_quality-$max_quality -- ".$file);
    }

    /**
     * Optimize JPG with jpegoptim libary.
     *
     * apt-get install jpegoptim (linux)
     *
     * @param $file
     * @param int $quality
     */
    public function compressJpg($file, $quality = 75) {
        $file = escapeshellarg($file);
        return shell_exec("jpegoptim ".$file." -m".$quality." --strip-all");
    }

    /**
     * Post process images according to extension.
     *
     * @param $file
     * @param $extension
     */
    protected function postProcess($file, $extension) {
        switch($extension) {
            case 'png':
                $quality = $this->config->optimizer_png['pngquant'];
                Sourceset::debug('optimizing png: pngquant -> ' . $quality);
                $this->compressPng($file, $quality);
                break;
            case 'jpg':
            case 'jpeg':
                $quality = $this->config->optimizer_jpg['jpegoptim'];
                Sourceset::debug('optimizing jpg: jpegoptim -> ' . $quality);
                $this->compressJpg($file, $quality);
                break;
        }
    }

    /**
     * Rertrieve generated sizes from the database/cache
     *
     * @param string $filename
     * @return array
     */
    public function retrieveSourcesetModel() {
        // check if cache is enabled
        $cache_enabled = $this->config->cache['enabled'];
        if($cache_enabled && $this->cache->has($this->cache_prefix)) {
            Sourceset::debug('retrieving from cache');
            $model = $this->cache->get($this->cache_prefix);
            $model->from_cache = true;
            return $model;
        }

        $source = $this->source;
        $sizes = $this->sizes;
        $callback = function () use ($source, $sizes) {
            return SourcesetModel::findOrCreate($source->getSource(), $sizes);
        };

        // only for debug purposes
        if($cache_enabled) {
            Sourceset::debug('storing in cache');
        }

        return $cache_enabled ?
            $this->cache->remember($this->cache_prefix, $this->cache_remember * 60, $callback) :
            $callback();
    }

    /**
     * Store model
     *
     * @todo: check if model was changed before storing and saving to database/cache.
     *
     * @param array $sizes
     */
    protected function storeSourcesetModel() {
        Sourceset::debug('save to database');
        $this->image->orderSizesDesc();
        try {
            $this->image->save();
        } catch(\Exception $e) {
            Sourceset::debug('error storing in database: ' . $e->getMessage());
        }
        // store in cache if enabled
        if($this->config->cache['enabled']) {
            Sourceset::debug('save to cache');
            $this->cache->put($this->cache_prefix, $this->image, $this->cache_remember);
        }
    }

    /**
     * Remove generated sizes
     * @todo: implement
     */
    public function removeImage() {
        Sourceset::debug('@todo: implement');
    }

    /**
     * Flush the cache
     *
     * @return mixed
     */
    public function flushCache() {
        Sourceset::debug('flushing cache');
        return $this->cache->flush();
    }

    /**
     * @return array
     */
    public function getGeneratedData() {
        return [
            'data' => $this->image->data,
            'resizeables' => $this->image->resizeables
        ];
    }
}