<?php

namespace WizeWiz\ImageSourceset;

use WizeWiz\ImageSourceset\Exceptions\{
    InvalidSizesException,
    InvalidSourceException,
    NoSourceFileException
};
use WizeWiz\ImageSourceset\Models\Sourceset as SourcesetModel;
use WizeWiz\ImageSourceset\SourcesetGenerate;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class Sourceset {

    public static $debug = false;

    /**
     * Quite impossible to debug without it
     *
     * @param $msg
     */
    public static function debug($msg) {
        if(self::$debug) {
            if(is_string($msg)) {
                Log::info('ImageSourcset: '. $msg);
                return;
            }
            Log::info(': debug');
            Log::debug($msg);
        }
    }

    protected $source;
    protected $disk_name = 'local';
    protected $disk;
    protected $sizes;

    protected $data = [];
    protected $resizeables = [];

    protected $file;
    protected $file_url_path;

    /**
     * @var bool If source and sizes are valid.
     */
    protected $valid = false;

    /**
     * @var bool Silent will not quit any process but just return the source with path.
     */
    protected $silent = true;

    /**
     * Sourceset constructor.
     * @param $source
     * @param $sizes
     */
    public function __construct($source, array $sizes, $filter = null, $mode = null) {
        // set config
        $this->config = $this->setConfig(config('sourceset'));
        // set sizes
        $this->sizes = $sizes;
        // set generation mode
        $this->setMode($mode);
        Sourceset::debug(('mode: ' . $this->config->mode));
        // verbose output
        // @todo: rename to verbose
        self::$debug = $this->config->verbose;
        // silent mode
        $this->silent = $this->config->silent;

        try {
            // validate if source image exists
            if(is_array($source)) {
                $this->disk_name = $source[1];
                $source = $source[0];
            }

            $this->disk = $this->validateDisk($this->disk_name);
            $this->source = $this->validateSource($source);

            $this->file = (object)pathinfo($source);
            if(isset($this->file->filename) && isset($this->file->extension)) {
                $this->valid = true;
            }
        } catch(\Exception $e) {
            self::debug(get_class($e) .': '. $e->getMessage() . ' @ ' . $e->getFile() .' at ' . $e->getLine());

            if($this->config->silent === false && self::$debug) {
                var_dump(get_class($e) .': '. $e->getMessage());
                exit;
            }
            $this->source = $source;
            $this->valid = false;
        }

        if($this->valid) {
            list('data' => $this->data, 'resizeables' => $this->resizeables) =
                (new SourcesetGenerate($this, $sizes, $this->resolveFilter($filter)))
                    ->getGeneratedData();
        }
    }

    /**
     * Set configuration
     *
     * @param array $config
     * @return object
     */
    protected function setConfig(array $config) {
        $environment = App::environment();
        // overwrite settings per environment if available
        // @todo: this can be moved in the ServiceProvider::register with mergeConfigFrom
        if( array_key_exists('env', $config) &&
            is_array($config['env']) &&
            array_key_exists($environment, $config['env']) &&
            is_array($config['env'][$environment])) {
                Sourceset::debug('config: overwriting for environment ' . $environment);
                $config = array_merge($config, $config['env'][$environment]);
        }
        return (object) $config;
    }

    /**
     * Get config.
     *
     * @return object
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * Set check mode.
     *
     * @param $mode
     * @return mixed
     */
    protected function setMode($mode) {
        $mode or $mode = $this->config->mode;
        $this->config->mode = (function($mode) {
            switch($mode) {
                // verify if size filename exists
                default:
                case 'verify':
                    return $mode;
                    break;
                // calculate difference of stored sizes against given sizes, confident all sizes
                // in the database entry are generated
                case 'confident':
                    return $mode;
                    break;
            }
        })($mode);
    }

    /**
     * @return mixed
     */
    protected function getURLPath($path) {
        return str_replace(public_path(), '', $path);
    }

    /**
     * Get basename.
     *
     * @return string
     */
    public function getDirname() {
        if($this->valid)
            return $this->file->dirname;
    }

    /**
     * Get basename.
     *
     * @return string
     */
    public function getBasename() {
        if($this->valid)
            return $this->file->basename;
    }

    /**
     * Get filename.
     *
     * @return mixed
     */
    public function getFilename() {
        if($this->valid)
            return $this->file->filename;
    }

    /**
     * Get file extension.
     *
     * @return mixed
     */
    public function getExtension() {
        if($this->valid)
            return strtolower($this->file->extension);
    }

    /**
     * Get source.
     *
     * @return mixed
     */
    public function getSource() {
        return $this->source;
    }

    /**
     * Get path to file.
     *
     * @return mixed
     */
    public function getFilePath() {
        return $this->disk->path($this->source);
    }


    /**
     * Resolve filter key.
     *
     * @param $filter
     * @return mixed
     */
    protected function resolveFilter($filter) {
        if($filter === null || empty($this->config->filters) || !isset($this->config->filters[$filter])) {
            return $this->config->default_filter;
        }
        return $this->config->filters[$filter];
    }

    /**
     * Validate the given disk.
     * @todo: validation (try/catch) here.
     *
     * @param $disk
     * @return mixed
     */
    protected function validateDisk($disk) {
        return Storage::disk($disk);
    }

    /**
     * Determine the source.
     *
     * @todo: add File support
     * @todo: add image/intervention Image support
     * @note: currently only string:path.jpg
     * @param $source
     * @throws NoSourceFileException
     * @throws InvalidSourceException
     * @return string
     */
    protected function validateSource($source) {
        if(!is_string($source)) {
            throw new InvalidSourceException('source should be of type string');
        }
        if(!$this->disk->has($source)) {
            throw new NoSourceFileException('source file does not exist: ' . $source . ' @ disk: ' . $this->disk_name);
        }
        return $source;
    }

    /**
     * Format sizes as array, size will be key and resized image name will be value.
     *
     * @return array
     */
    public function asArray() : array {
        $arr = [];

        if(!$this->valid) {
            if($this->silent) {
                return $this->getRelativePath();
            }
            return $arr;
        }

        if(!empty($this->resizeables)) {

            $storage_path = $this->resizeables['paths']['storage'];

            foreach ($this->resizeables['resizeables'] as $size => $resized_image) {
                $arr[$size] = "{$storage_path}{$resized_image}";
            }
        }

        return $arr;
    }

    /**
     * Collect defined $sizes from resizeables array
     *
     * @param $sizes
     * @return array
     */
    public function fromArray($sizes) {
        $collected = [];

        if(!$this->valid) {
            if($this->silent) {
                return $this->getRelativePath();
            }
            return $collected;
        }

        if(!is_array($sizes)) {
            $sizes = [$sizes];
        }
        if(!empty($this->resizeables)) {
            $storage_path = $this->data['paths']['storage'];

            foreach ($sizes as $size) {
                if (isset($this->resizeables[$size])) {
                    $collected[$size] = $storage_path.$this->resizeables[$size];
                }
            }
        }
        return $collected;
    }

    /**
     * Generate sizes as srcset string.
     *
     * @return string
     */
    public function asSourceset() : string {
        $str = '';

        if(!$this->valid) {
            if($this->silent) {
                return $this->getRelativePath();
            }
            return $str;
        }

        if(!empty($this->resizeables)) {
            $storage_path = $this->data['paths']['storage'];
            $position = 0;
            foreach($this->sizes as $size) {
                if(!isset($this->resizeables[$size])) {
                    Sourceset::debug("skipping {$size}, in generated (or stored) resizeable");
                    continue;
                }

                $resized_image = $this->resizeables[$size];

                if($position > 0) {
                    $str .= ', ';
                }

                $str .= "{$storage_path}{$resized_image} {$size}w";

                $position++;
            }
        }

        return !empty($str) ?
            // generated sourceset string
            $str :
            // if sourceset could not be generated, return original as last resort
            $this->getRelativePath();
    }

    /**
     * Get a specific resizeable of an image.
     *
     * @param $size
     * @return string|null
     */
    public function getResizeable($size) {
        if(!$this->valid) {
            if($this->silent) {
                return $this->getRelativePath();
            }
            return null;
        }

        $storage_path = $this->data['paths']['storage'];

        return $storage_path.$this->resizeables[$size];
    }

    /**
     * Get relative path to image.
     */
    public function getRelativePath() {
        if($this->disk !== null && $this->disk->has($this->source)) {
            return str_replace(url(''), '', $this->disk->url($this->source));
        }
        // return an empty string so it never fails
        return config('sourceset.image_not_found', '');
    }

    /**
     * Outputs the string representation of a source-set (srcset).
     *
     * @return string
     */
    public function __toString() {
        return $this->asSourceset();
    }

}