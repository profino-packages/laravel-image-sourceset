<?php

namespace WizeWiz\ImageSourceset\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use WizeWiz\ImageSourceset\Sourceset as SourcesetBase;

class Sourceset extends Model {

    protected $table = 'image_sourcesets';

    public $from_cache = false;

    protected $casts = [
        'data' => 'array',
        'resizeables' => 'array'
    ];

    protected $fillable = [
        'filename',     // literal filename without path and extensions
        'file',         // file path
        'filter',       // used filter
        'data',         // source data
        'resizeables'   // resizeables available as array
    ];

    protected $visible = [
        'filename',
        'file',
        'filter',
        'data',
        'resizeables'
    ];

    public $timestamps = true;

    /**
     * @param $value
     * @return object
     */
    public function getFileAttribute($value) {
        $file = (object) pathinfo($value);
        $file->dirname = $file->dirname . DIRECTORY_SEPARATOR;
        $file->source = $value;
        return $file;
    }

    /**
     * @param $filter
     * @param $sizes
     * @param $config
     */
    public function initialize($filter, $sizes, array $paths) {
        $this->filter = $filter;

        $resizeables = [];
        foreach($sizes as $size) {
            $resizeables[$size] = $this->generateResizeableFilename($size, true);
        }

        $this->data = [
            'sizes' => $sizes,
            'extension' => $this->file->extension,
            'paths' => $paths,
        ];

        $this->resizeables = $resizeables;
    }

    /**
     * Generate sized filename according to $size.
     *
     * @param int $size
     * @param boolean $include_dirname Include the directory name to file.
     * @return string
     */
    public function generateResizeableFilename(int $size, $include_dirname = false) {
        $filename = $this->file->filename .'_'.$size.'.'.$this->file->extension;

        return $include_dirname ?
            $this->file->dirname . $filename :
            $filename;
    }

    /**
     * Get resizeables for image
     */
    public function getResizeables() {
        return (array) $this->resizeables;
    }

    /**
     * Calculate difference and return those that are missing
     *
     * @param array $sizes
     */
    public function getMissingResizeables(array $sizes) {
        return array_diff($sizes, $this->sizes ?? []);
    }

    /**
     * Order sizes from biggest size to smallest
     *
     * @return mixed
     */
    public function orderSizesDesc() {
        SourcesetBase::debug('order sizes desc');
        $data = $this->data;
        asort($data['sizes']);
        $data['sizes'] = array_reverse($data['sizes']);
        return $this->data = $data;
    }

    /**
     * Set resizeable
     *
     * @param int $size The width of the image
     * @param string $resizeable_filename Resizeable name
     */
    public function setResizeable(int $size, string $resizeable_filename) {
        // get data first before we modify
        $data = $this->data;
        // add resizeable
        $resizeables = $this->resizeables;
        $resizeables[$size] = $resizeable_filename;
        $this->resizeables = $resizeables;
        // add size if not present
        if(!in_array($size, $data['sizes'])) {
            $data['sizes'][] = $size;
        }
        // write data to attribute
        $this->data = $data;
    }

    /**
     * @param string $source
     */
    public static function findOrCreate(string $source) {
        $model = self::where('file', $source)
            ->select(
                'id',
                'filename',
                'file',
                'data',
                'resizeables',
                'filter',
                'created_at'
            )->first();
        // if model was not found
        if($model === null) {
            SourcesetBase::debug('no record found, creating model on the fly');
            // create one on the fly
            $model = Sourceset::make([
                'filename' => pathinfo($source)['filename'],
                'file' => $source
            ]);
        }

        return $model;
    }

    /**
     * Has record in the database
     *
     * @return bool
     */
    public function hasRecord() {
        return $this->id !== null;
    }

    /**
     * Generate a pretty random hash from the image
     */
    public static function generateHashedFileName(string $filename) {
        if(strpos($filename, '.', 0) === false) {
            throw new \Exception("given filename: {$filename} does not seem to have any extension.");
        }

        return md5( $filename.microtime() ).'.'.pathinfo($filename)['extension'];
    }

    /**
     * @param $hashed_filename
     * @return 0|array
     * @throws \Exception
     */
    public static function generateStorePath($hashed_filename, $separate = false) {
        if(strlen($hashed_filename) <= 10) {
            throw new \Exception("hashed image name: {$hashed_filename} is not suitable for path generation, increase string length.");
        }

        $path = implode(DIRECTORY_SEPARATOR, array_slice(str_split($hashed_filename), 0, 10));
        $filename = substr($hashed_filename, 10);

        return
            $separate ? [
                'path' => $path,
                'filename' => $filename,
                'filepath' => $path.DIRECTORY_SEPARATOR.$filename
            ] :
            $path.$filename;
    }

}
